/*
 * Copyright (c) 2018
 *	Side Effects Software Inc.  All rights reserved.
 *
 * Redistribution and use of Houdini Development Kit samples in source and
 * binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. The name of Side Effects Software may not be used to endorse or
 *    promote products derived from this software without specific prior
 *    written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY SIDE EFFECTS SOFTWARE `AS IS' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN
 * NO EVENT SHALL SIDE EFFECTS SOFTWARE BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *----------------------------------------------------------------------------
 * The InstantMeshes SOP
 */

#include "SOP_InstantMeshes.h"
 
// This is an automatically generated header file based on theDsFile, below,
// to provide SOP_InstantMeshesParms, an easy way to access parameter values from
// SOP_InstantMeshesVerb::cook with the correct type.
#include "SOP_InstantMeshes.proto.h"

#include <GU/GU_Detail.h>
#include <GEO/GEO_PrimPoly.h>
#include <OP/OP_Operator.h>
#include <OP/OP_OperatorTable.h>
#include <PRM/PRM_Include.h>
#include <PRM/PRM_TemplateBuilder.h>
#include <UT/UT_DSOVersion.h>
#include <UT/UT_Interrupt.h>
#include <UT/UT_StringHolder.h>
#include <SYS/SYS_Math.h>
#include <limits.h>
#include <GA/GA_PolyCounts.h>

#include "Ibatch.h"

using namespace InstantMeshesH;

//
// Help is stored in a "wiki" style text file.  This text file should be copied
// to $HOUDINI_PATH/help/nodes/sop/star.txt
//
// See the sample_install.sh file for an example.
//

/// This is the internal name of the SOP type.
/// It isn't allowed to be the same as any other SOP's type name.
const UT_StringHolder SOP_InstantMeshes::theSOPTypeName("hdk_InstantMeshes"_sh);

/// newSopOperator is the hook that Houdini grabs from this dll
/// and invokes to register the SOP.  In this case, we add ourselves
/// to the specified operator table.
const char *inputLabels[] = { "IN_GEO", nullptr };
void
newSopOperator(OP_OperatorTable *table)
{
    table->addOperator(new OP_Operator(
        SOP_InstantMeshes::theSOPTypeName,   // Internal name
        "InstantMeshes",                     // UI name
        SOP_InstantMeshes::myConstructor,    // How to build the SOP
        SOP_InstantMeshes::buildTemplates(), // My parameters
        1,                          // Min # of sources
        1,                          // Max # of sources
        nullptr,                    // Custom local variables (none)
        OP_FLAG_GENERATOR,			// Flag it as generator
		inputLabels));        
}

/// This is a multi-line raw string specifying the parameter interface
/// for this SOP.
static const char *theDsFile = R"THEDSFILE(
{
    name        parameters
	parm {
		name	"res"
		label	"Resolution (faces)"
		type	integer
		default	{ "1200" }
		range	{ 100! 100000 }
		export	all
	}
	parm {
		name	"mode"
		label	"Mode"
		type	ordinal
		default	{ "0" }
		menu	{
			"extrins"	"Extrinsic"
			"intrins"	"Intrinsic"
		}
	}
	parm {
		name	"bound"
		label	"Align to Boundaries"
		type	toggle
		default	{ "0" }
	}
	parm {
		name	"determ"
		label	"Deterministic"
		type	toggle
		default	{ "0" }
	}
	parm {
		name	"remesh"
		label	"Remesh as"
		type	ordinal
		default	{ "2" }
		menu	{
			"66"	"Triangles (6-RoSy, 6-PoSy)"
			"24"	"Quads (2-RoSy, 4-PoSy)"
			"44"	"Quads (4-RoSy, 4-PoSy)"
		}
	}
	parm {
		name	"smooth"
		label	"Smoothing Steps"
		type	integer
		default	{ "0" }
		range	{ 0! 20 }
		export	all
	}
	parm {
		name	"logging"
		label	"Log Output"
		type	toggle
		default	{ "0" }
	}
}
)THEDSFILE";

PRM_Template*
SOP_InstantMeshes::buildTemplates()
{
    static PRM_TemplateBuilder templ("SOP_InstantMeshes.cpp"_sh, theDsFile);
    return templ.templates();
}

class SOP_InstantMeshesVerb : public SOP_NodeVerb
{
public:
    SOP_InstantMeshesVerb() {}
    virtual ~SOP_InstantMeshesVerb() {}

    virtual SOP_NodeParms *allocParms() const { return new SOP_InstantMeshesParms(); }
    virtual UT_StringHolder name() const { return SOP_InstantMeshes::theSOPTypeName; }

    virtual CookMode cookMode(const SOP_NodeParms *parms) const { return COOK_GENERIC; }

    virtual void cook(const CookParms &cookparms) const;
    
    /// This static data member automatically registers
    /// this verb class at library load time.
    static const SOP_NodeVerb::Register<SOP_InstantMeshesVerb> theVerb;
};

// The static member variable definition has to be outside the class definition.
// The declaration is inside the class.
const SOP_NodeVerb::Register<SOP_InstantMeshesVerb> SOP_InstantMeshesVerb::theVerb;

const SOP_NodeVerb *
SOP_InstantMeshes::cookVerb() const 
{ 
    return SOP_InstantMeshesVerb::theVerb.get();
}

/// This is the function that does the actual work.
void
SOP_InstantMeshesVerb::cook(const SOP_NodeVerb::CookParms &cookparms) const
{
    auto &&sopparms = cookparms.parms<SOP_InstantMeshesParms>();
    GU_Detail *detail = cookparms.gdh().gdpNC();

	const GU_Detail * const inputGeo = cookparms.inputGeo(0);	//	input mesh
	GA_Size pointCount = inputGeo->getNumPoints();

	std::vector<float> pointPositions;

	UT_Array<UT_Vector3> positionArray;
	inputGeo->getPos3AsArray(inputGeo->getPointRange(), positionArray);
	for (exint i = 0; i < positionArray.size(); ++i)
	{
		UT_Vector3 v = positionArray(i);
		pointPositions.push_back(v[0]);
		pointPositions.push_back(v[1]);
		pointPositions.push_back(v[2]);
	}

	std::vector<uint32_t> triangleIndices;
	const GEO_Primitive * prim;
	for (GA_Iterator it(inputGeo->getPrimitiveRange()); !it.atEnd(); ++it)
	{
		prim = inputGeo->getGEOPrimitive(*it);
		GA_Size vCount = prim->getVertexCount();

		for (int t = 0; t < vCount - 2; ++t)	//	fan-triangulate polygons
		{
			GA_Size v[] = { 0, 1 + t, 2 + t };
			for (const GA_Size& i : v)
			{
				GA_Offset vtxoff = prim->getVertexOffset(i);
				GA_Offset ptoff = inputGeo->vertexPoint(vtxoff);
				GA_Index ptidx = inputGeo->pointIndex(ptoff);
				triangleIndices.push_back((uint32_t)ptidx);
			}
		}
	}


	std::unique_ptr<Ibatch> iInstantMeshes = Ibatch::createBatchImpl();

	int rosy, posy;
	int symmetryType = (int)sopparms.getRemesh();
	switch (symmetryType)
	{
	case 0:
		rosy = 6;
		posy = 3;
		break;
	case 1:
		rosy = 2;
		posy = 4;
		break;
	case 2:
		rosy = 4;
		posy = 4;
		break;
	default:
		rosy = 2;
		posy = 4;
		break;
	}
	
	iInstantMeshes->setParams((int)sopparms.getRes(), !(bool)sopparms.getMode(), false, (bool)sopparms.getBound(), (bool)sopparms.getDeterm(),
		rosy, posy, (int)sopparms.getSmooth(), (bool)sopparms.getLogging());

	iInstantMeshes->setInputGeometry((int)pointCount, pointPositions.data(), (int)triangleIndices.size() / 3, triangleIndices.data());
	iInstantMeshes->process();
	long long outPointCount = 0;
	long long outFaceCols = 0;
	int outFaceRows = 0;
	iInstantMeshes->getOutputGeometrySize(outPointCount, outFaceCols, outFaceRows);
	std::vector<fpreal32> outPointPositions(outPointCount * 3, 0.0f);
	std::vector<int> outFaceIndices(outFaceCols * outFaceRows, -1);
	iInstantMeshes->getOutputGeometry(outPointPositions.data(), outFaceIndices.data());


	detail->clearAndDestroy();	//	will always regenerate

	GA_Offset start_ptoff = detail->appendPointBlock(outPointCount);

	GA_PolyCounts polyCounts;
	std::vector<GA_Size> faceIndices;

	std::map<uint32_t, std::pair<uint32_t, std::map<uint32_t, uint32_t>>> irregular;
	GA_Size nIrregular = 0;
	GA_Size nRegular = 0;

	for (uint32_t f = 0; f < outFaceCols; ++f)
	{
		if (outFaceRows == 4)
		{
			if (outFaceIndices[f * outFaceRows + 2] == outFaceIndices[f * outFaceRows + 3])		//	marker for "irregular" face
			{
				nIrregular++;
				auto &value = irregular[outFaceIndices[f * outFaceRows + 2]];	// outFaceIndices[f * outFaceRows + 2] is the key for this irregular face
				value.first = f;
				value.second[outFaceIndices[f * outFaceRows]] = outFaceIndices[f * outFaceRows + 1];	//	encodes single edge of this irregular face
				continue;
			}
		}

		//	regular face:
		polyCounts.append(outFaceRows, 1);
		for (uint32_t j = 0; j < outFaceRows; ++j) 
		{
			faceIndices.push_back(outFaceIndices[f * outFaceRows + j]);
		}
	}

	//	irregular faces
	for (auto& item : irregular) 
	{
		auto face = item.second;
		uint32_t v = face.second.begin()->first, first = v, i = 0;

		while (true) 
		{
			faceIndices.push_back(v);

			v = face.second[v];
			if (v == first || ++i == face.second.size())
				break;
		}
		polyCounts.append(face.second.size(), 1);
	}

	GA_Offset start_vtxoff;
	detail->appendPrimitivesAndVertices(GA_PRIMPOLY, polyCounts, start_vtxoff, true);
	for (GA_Size i = 0; i < polyCounts.getNumVertices(); ++i)
	{
		detail->getTopology().wireVertexPoint(start_vtxoff + i, start_ptoff + faceIndices[i]);
	}
	detail->bumpDataIdsForAddOrRemove(true, true, true);

	for (exint i = 0; i < outPointCount; ++i)
	{
		UT_Vector3 v(outPointPositions[3 * i], outPointPositions[3 * i + 1], outPointPositions[3 * i + 2]);
		GA_Offset ptoff = start_ptoff + i;
		detail->setPos3(ptoff, v);
	}


 /*   // We need two points per division
    exint npoints = sopparms.getDivs()*2;

    if (npoints < 4)
    {
        // With the range restriction we have on the divisions, this
        // is actually impossible, (except via integer overflow),
        // but it shows how to add an error message or warning to the SOP.
        cookparms.sopAddWarning(SOP_MESSAGE, "There must be at least 2 divisions; defaulting to 2.");
        npoints = 4;
    }

    // If this SOP has cooked before and it wasn't evicted from the cache,
    // its output detail will contain the geometry from the last cook.
    // If it hasn't cooked, or if it was evicted from the cache,
    // the output detail will be empty.
    // This knowledge can save us some effort, e.g. if the number of points on
    // this cook is the same as on the last cook, we can just move the points,
    // (i.e. modifying P), which can also save some effort for the viewport.

    GA_Offset start_ptoff;
    if (detail->getNumPoints() != npoints)
    {
        // Either the SOP hasn't cooked, the detail was evicted from
        // the cache, or the number of points changed since the last cook.

        // This destroys everything except the empty P and topology attributes.
        detail->clearAndDestroy();

        // Build 1 closed polygon (as opposed to a curve),
        // namely that has its closed flag set to true,
        // and the right number of vertices, as a contiguous block
        // of vertex offsets.
        GA_Offset start_vtxoff;
        detail->appendPrimitivesAndVertices(GA_PRIMPOLY, 1, npoints, start_vtxoff, true);

        // Create the right number of points, as a contiguous block
        // of point offsets.
        start_ptoff = detail->appendPointBlock(npoints);

        // Wire the vertices to the points.
        for (exint i = 0; i < npoints; ++i)
        {
            detail->getTopology().wireVertexPoint(start_vtxoff+i,start_ptoff+i);
        }

        // We added points, vertices, and primitives,
        // so this will bump all topology attribute data IDs,
        // P's data ID, and the primitive list data ID.
        detail->bumpDataIdsForAddOrRemove(true, true, true);
    }
    else
    {
        // Same number of points as last cook, and we know that last time,
        // we created a contiguous block of point offsets, so just get the
        // first one.
        start_ptoff = detail->pointOffset(GA_Index(0));

        // We'll only be modifying P, so we only need to bump P's data ID.
        detail->getP()->bumpDataId();
    }

    // Everything after this is just to figure out what to write to P and write it.

    const SOP_InstantMeshesParms::Orient plane = sopparms.getOrient();
    const bool allow_negative_radius = sopparms.getNradius();

    UT_Vector3 center = sopparms.getT();

    int xcoord, ycoord, zcoord;
    switch (plane)
    {
        case SOP_InstantMeshesParms::Orient::XY:         // XY Plane
            xcoord = 0;
            ycoord = 1;
            zcoord = 2;
            break;
        case SOP_InstantMeshesParms::Orient::YZ:         // YZ Plane
            xcoord = 1;
            ycoord = 2;
            zcoord = 0;
            break;
        case SOP_InstantMeshesParms::Orient::ZX:         // XZ Plane
            xcoord = 0;
            ycoord = 2;
            zcoord = 1;
            break;
    }

    // t the interrupt scope
    UT_AutoInterrupt boss("Building InstantMeshes");
    if (boss.wasInterrupted())
        return;

    float tinc = M_PI*2 / (float)npoints;
    float outer_radius = sopparms.getRad().x();
    float inner_radius = sopparms.getRad().y();
    
    // Now, set all the points of the polygon
    for (exint i = 0; i < npoints; i++)
    {
        // Check to see if the user has interrupted us...
        if (boss.wasInterrupted())
            break;

        float angle = (float)i * tinc;
        bool odd = (i & 1);
        float rad = odd ? inner_radius : outer_radius;
        if (!allow_negative_radius && rad < 0)
            rad = 0;

        UT_Vector3 pos(SYScos(angle)*rad, SYSsin(angle)*rad, 0);
        // Put the circle in the correct plane.
        pos = UT_Vector3(pos(xcoord), pos(ycoord), pos(zcoord));
        // Move the circle to be centred at the correct position.
        pos += center;

        // Since we created a contiguous block of point offsets,
        // we can just add i to start_ptoff to find this point offset.
        GA_Offset ptoff = start_ptoff + i;
        detail->setPos3(ptoff, pos);
    }*/
}

/*
	batch.cpp -- command line interface to Instant Meshes

	This file is part of the implementation of

		Instant Field-Aligned Meshes
		Wenzel Jakob, Daniele Panozzo, Marco Tarini, and Olga Sorkine-Hornung
		In ACM Transactions on Graphics (Proc. SIGGRAPH Asia 2015)

	All rights reserved. Use of this source code is governed by a
	BSD-style license that can be found in the LICENSE.txt file.
*/

#include "Ibatch.h"
#include "meshio.h"
#include "dedge.h"
#include "subdivide.h"
#include "meshstats.h"
#include "hierarchy.h"
#include "field.h"
#include "normal.h"
#include "extract.h"
#include "bvh.h"



class Batch : public Ibatch
{
public:
	void setInputGeometry(long pointCount, float * positions, long triangleCount, uint32_t * triangleIndices) override;
	void setParams(int setFaces, bool setExtrinsic, bool setPureQuad, bool setAlignBoundaries, bool setDeterministic, int setRosy, int setPosy, int setSmoothSteps, bool setLogging) override;
	void process() override;
	void getOutputGeometrySize(long long& pointCount, long long& faceCols, int& faceRows) override;
	void getOutputGeometry(float * positions, int * indices) override;

private:
	bool extrinsic = true;
	bool pure_quad = false;
	bool align_to_boundaries = false;
	bool fullscreen = false;
	bool help = false;
	bool deterministic = false;
	bool compat = false;
	int rosy = 4;
	int posy = 4;
	int face_count = -1;
	int vertex_count = -1;
	uint32_t knn_points = 10;
	uint32_t smooth_iter = 0;
	Float creaseAngle = -1;
	Float scale = -1;

	bool pointcloud = false;

	MatrixXu F;
	MatrixXf V, N;
	VectorXf A;
	std::set<uint32_t> crease_in, crease_out;
	BVH *bvh = nullptr;
	AdjacencyMatrix adj = nullptr;

	MatrixXf O_extr, N_extr, Nf_extr;
	MatrixXu F_extr;
};

bool logging = false;


void Batch::setInputGeometry(long pointCount, float * positions, long triangleCount, uint32_t * triangleIndices)
{
	F.resize(3, triangleCount);
	for (uint32_t i = 0; i < triangleCount; ++i)
	{
		for (int j = 0; j < 3; ++j)
		{
			F(j, i) = triangleIndices[3 * i + j];
		}
	}

	V.resize(3, pointCount);
	for (long long i = 0; i < pointCount; ++i)
		V.col(i) = Eigen::Vector3f(positions[3 * i], positions[3 * i + 1], positions[3 * i + 2]);

	return;
}

void Batch::setParams(int setFaces, bool setExtrinsic, bool setPureQuad, bool setAlignBoundaries, bool setDeterministic, int setRosy, int setPosy, int setSmoothSteps, bool setLogging)
{
	logging = setLogging;
	face_count = setFaces;
	extrinsic = setExtrinsic;
	pure_quad = setPureQuad;
	align_to_boundaries = setAlignBoundaries;
	deterministic = setDeterministic;
	rosy = setRosy;
	posy = setPosy;
	smooth_iter = setSmoothSteps;
}

void Batch::process()
{
	MeshStats stats = compute_mesh_stats(F, V, deterministic);
	Float face_area = stats.mSurfaceArea / face_count;
	vertex_count = posy == 4 ? face_count : (face_count / 2);
	scale = posy == 4 ? std::sqrt(face_area) : (2 * std::sqrt(face_area * std::sqrt(1.f / 3.f)));

	MultiResolutionHierarchy mRes;

	if (!pointcloud) {
		/* Subdivide the mesh if necessary */
		VectorXu V2E, E2E;
		VectorXb boundary, nonManifold;
		if (stats.mMaximumEdgeLength * 2 > scale || stats.mMaximumEdgeLength > stats.mAverageEdgeLength * 2) {
			if (logging) {
				cout << "Input mesh is too coarse for the desired output edge length "
					"(max input mesh edge length=" << stats.mMaximumEdgeLength
					<< "), subdividing .." << endl;
			}
			build_dedge(F, V, V2E, E2E, boundary, nonManifold);
			subdivide(F, V, V2E, E2E, boundary, nonManifold, std::min(scale / 2, (Float)stats.mAverageEdgeLength * 2), deterministic);
		}

		/* Compute a directed edge data structure */
		build_dedge(F, V, V2E, E2E, boundary, nonManifold);

		/* Compute adjacency matrix */
		adj = generate_adjacency_matrix_uniform(F, V2E, E2E, nonManifold);

		/* Compute vertex/crease normals */
		if (creaseAngle >= 0)
			generate_crease_normals(F, V, V2E, E2E, boundary, nonManifold, creaseAngle, N, crease_in);
		else
			generate_smooth_normals(F, V, V2E, E2E, nonManifold, N);

		/* Compute dual vertex areas */
		compute_dual_vertex_areas(F, V, V2E, E2E, nonManifold, A);

		mRes.setE2E(std::move(E2E));
	}

	/* Build multi-resolution hierarrchy */
	mRes.setAdj(std::move(adj));
	mRes.setF(std::move(F));
	mRes.setV(std::move(V));
	mRes.setA(std::move(A));
	mRes.setN(std::move(N));
	mRes.setScale(scale);
	mRes.build(deterministic);
	mRes.resetSolution();

	if (align_to_boundaries && !pointcloud) {
		mRes.clearConstraints();
		for (uint32_t i = 0; i < 3 * mRes.F().cols(); ++i) {
			if (mRes.E2E()[i] == INVALID) {
				uint32_t i0 = mRes.F()(i % 3, i / 3);
				uint32_t i1 = mRes.F()((i + 1) % 3, i / 3);
				Vector3f p0 = mRes.V().col(i0), p1 = mRes.V().col(i1);
				Vector3f edge = p1 - p0;
				if (edge.squaredNorm() > 0) {
					edge.normalize();
					mRes.CO().col(i0) = p0;
					mRes.CO().col(i1) = p1;
					mRes.CQ().col(i0) = mRes.CQ().col(i1) = edge;
					mRes.CQw()[i0] = mRes.CQw()[i1] = mRes.COw()[i0] =
						mRes.COw()[i1] = 1.0f;
				}
			}
		}
		mRes.propagateConstraints(rosy, posy);
	}

	if (bvh) {
		bvh->setData(&mRes.F(), &mRes.V(), &mRes.N());
	}
	else if (smooth_iter > 0) {
		bvh = new BVH(&mRes.F(), &mRes.V(), &mRes.N(), stats.mAABB);
		bvh->build();
	}

	if (logging) { cout << "Preprocessing is done." << endl; }

	Optimizer optimizer(mRes, false);
	optimizer.setRoSy(rosy);
	optimizer.setPoSy(posy);
	optimizer.setExtrinsic(extrinsic);

	if (logging) { cout << "Optimizing orientation field .. "; }
	if (logging) { cout.flush(); }
	optimizer.optimizeOrientations(-1);
	optimizer.notify();
	optimizer.wait();

	std::map<uint32_t, uint32_t> sing;
	compute_orientation_singularities(mRes, sing, extrinsic, rosy);
	if (logging) { cout << "Orientation field has " << sing.size() << " singularities." << endl; }

	if (logging) { cout << "Optimizing position field .. " << endl; }
	if (logging) { cout.flush(); }
	optimizer.optimizePositions(-1);
	optimizer.notify();
	optimizer.wait();
	if (logging) { cout << "done. " << endl; }

	//std::map<uint32_t, Vector2i> pos_sing;
	//compute_position_singularities(mRes, sing, pos_sing, extrinsic, rosy, posy);
	//if(logging){cout << "Position field has " << pos_sing.size() << " singularities." << endl;}
	//timer.reset();

	optimizer.shutdown();

	//MatrixXf O_extr, N_extr, Nf_extr;
	std::vector<std::vector<TaggedLink>> adj_extr;
	extract_graph(mRes, extrinsic, rosy, posy, adj_extr, O_extr, N_extr,
		crease_in, crease_out, deterministic);

	//MatrixXu F_extr;
	extract_faces(adj_extr, O_extr, N_extr, Nf_extr, F_extr, posy,
		mRes.scale(), crease_out, true, pure_quad, bvh, smooth_iter);
}

void Batch::getOutputGeometrySize(long long& pointCount, long long& faceCols, int& faceRows)
{
	pointCount = O_extr.cols();
	faceRows = F_extr.rows();
	faceCols = F_extr.cols();
}

void Batch::getOutputGeometry(float * positions, int * indices)
{
	int rows = F_extr.rows();
	for (size_t i = 0; i < F_extr.cols(); ++i)
	{
		for (int j = 0; j < rows; ++j)
		{
			indices[rows * i + j] = F_extr(j, i);
		}
	}

	for (size_t i = 0; i < O_extr.cols(); ++i)
	{
		for (int j = 0; j < 3; ++j)
		{
			positions[3 * i + j] = O_extr(j, i);
		}
	}
}

std::unique_ptr<Ibatch> Ibatch::createBatchImpl()
{
	return std::unique_ptr<Ibatch>(new Batch);
}



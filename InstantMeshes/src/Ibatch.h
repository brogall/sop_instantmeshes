#pragma once

#include <memory>


class Ibatch
{
public:
	virtual ~Ibatch() = default;

	virtual void setInputGeometry(long pointCount, float * positions, long triangleCount, uint32_t * triangleIndices) = 0;
	virtual void setParams(int setFaces, bool setExtrinsic, bool setPureQuad, bool setAlignBoundaries, bool setDeterministic, int setRosy, int setPosy, int setSmoothSteps, bool setLogging) = 0;
	virtual void process() = 0;
	virtual void getOutputGeometrySize(long long& pointCount, long long& faceCols, int& faceRows) = 0;
	virtual void getOutputGeometry(float * positions, int * indices) = 0;

	static std::unique_ptr<Ibatch> createBatchImpl();
};